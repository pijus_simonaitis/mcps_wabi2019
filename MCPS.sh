#!/bin/bash

start=`date +%s`

if [ "$1" == "-h" ] ; then
    echo "Usage: `basename $0` #ofgenesinablock genome1 genome2 orthology genomename1 genomename2" 
    exit 0
fi

genomename1=$5
genomename2=$6
name=$genomename1"_"$genomename2
name1="l$1_$name" 
name2="oc_$name1" 
blockinfo="oc_l$1"

cd syntenic_blocks/orthocluster 
./modifyBIOMARTforOrthocluster ../../$2 ../../$3 ../../$4 input/$name/ 0

mkdir -p output
mkdir -p output/$name1/
cd output/$name1/

echo
echo "1) Running OrthoCluster:"
echo
../../../bin/OrthoCluster -l $1 -rs -f -d $name1 -g1 ../../input/$name/genome1.txt -g2 ../../input/$name/genome2.txt -m ../../input/$name/ortho.txt

echo
echo "2) Transforming OrthoCluster gene clusters into syntenic blocks:"
echo
cd ../../
./readOrthocluster output/$name1/$name1.cluster output/$name1/$name1.log $name2 ../../data/blocks/


echo "3) Constructing breakpoint graph and computing MCPS"
echo

cd ../../mcps/
./picklegenomepair ../data/blocks/$name2.tsv $blockinfo $genomename1 $genomename2
./genomestocircles data/genomepairs/$blockinfo"_"$genomename1"_"$genomename2".pickle" 0

end=`date +%s`
runtime=$((end-start))
echo 
echo "total runtime is" $runtime "seconds"
