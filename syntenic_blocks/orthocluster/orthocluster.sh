#!/bin/bash


if [ "$1" == "-h" ] ; then
    echo "Usage: `basename $0` #ofgenesinablock genome1 genome2 orthology name" 
    exit 0
fi

name="$5"
name1="l$1_$name" 
name2="oc_$name1" 

./modifyBIOMARTforOrthocluster $2 $3 $4 input/$name/ 0

mkdir -p output
mkdir -p output/$name1/
cd output/$name1/

../../../bin/OrthoCluster -l $1 -rs -f -d $name1 -g1 ../../input/$name/genome1.txt -g2 ../../input/$name/genome2.txt -m ../../input/$name/ortho.txt

cd ../../

./readOrthocluster output/$name1/$name1.cluster output/$name1/$name1.log $name2 ../data/blocks/

