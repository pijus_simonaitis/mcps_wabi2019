#!/usr/bin/env python3
# Pijus Simonaitis                                               Spring 2018
#

"""
Library for syntenic block and their sets.

"""
from collections import defaultdict
import itertools
import csv
import re
import sys
import copy
import numpy as np

class Block(object):
  """
  Information on Block.

  @ivar left: the sup extremity of the block lying to the left from this block in genome 2.
              It is only used for those blocks that are at the extremities of chromosomes in genome 1
              but not in genome 2. 
  @type left: integer. 
  @ivar right: the inf extremity of the block lying to the right from the block in the genome 2.
               It is only used for those blocks that are at the extremities of chromosomes in genome 1
               but not in genome 2.
  @type right: integer
  @ivar genes1: set of genes in the syntenic block in genome1. These are only used with OrthoCluster.

  """

  def __init__(self):
    self.id = ""
    self.c1 = ""
    self.inf1 = None
    self.sup1 = None
    self.strand1 = 1 
    self.c2 = ""
    self.inf2 = None
    self.sup2 = None
    self.strand2 = None
    
    self.left = None 
    self.right = None
    self.genes1 = set()
    self.genes2 = set()

#        ________________________________________________
#_______/    Ordering of blocks in genome 2              \________________________________________________

  def __eq__(self, b):
    if self.c2 != b.c2:
      sys.exit('Blocks from different chromosomes are being compared')
    sys.exit('Blocks {} and block {} are equal.'.format(self.id,b.id))
    return(self.inf2 == b.inf2 and self.sup2 == b.sup2)

  def __lt__(self,b):
    """
    From the breakpoints provided by cassis we can not infer certain block extremities in genome2.
    They are left to be None.
    This function is used to sort these block with possibly one of the coordinates being equal to None.
    This ordering then allows us to infer the positions of the extremities using the block.left and block.right information.

    @note: there is one case in which the ordering of the blocks can not be inferred with certainty.
           if this case occurs, user is informed by a message, and we keep the most probable ordering.
    """

    if self.c2 != b.c2:
      sys.exit('Blocks from different chromosomes are being compared')
    if (self.inf2 == None and self.sup2 == None) or (b.inf2 == None and b.sup2 == None):
      sys.exit('Both block extremities are None in either block {} or block {}'.format(self.id,b.id))

    if self.inf2 == None:
      if b.sup2 == None:
        if self.sup2 > b.inf2:
          print("\nBlocks:")
          b.printblock()
          self.printblock()
          print("are being ordered. We chose to position the first before the second,")
          print("however the other way might also be possible and this chouce would lead to the incorrect syntenic blocks.\n") 
        return(self.sup2 < b.inf2)
      else:
        return(self.sup2 < b.sup2)
    elif self.sup2 == None:
      if b.inf2 == None:
        if b.sup2 > self.inf2:
          print("\nBlocks:")
          self.printblock()
          b.printblock()
          print("are being ordered. We chose to position the first before the second,")
          print("however the other way might also be possible and this chouce would lead to the incorrect syntenic blocks.\n")  
        return(self.inf2 < b.sup2)
      else:
        return(self.inf2 < b.inf2)
    else:
      if b.inf2 == None:
        return(self.sup2 < b.sup2)
      else:
        return(self.inf2 < b.inf2)

#        __________________________________
#_______/    Innitialization               \________________________________________________

  def innitializewithblockline(self, blockline):
    self.id = blockline[0]
    self.c1 = blockline[1]
    self.inf1 = int(blockline[2])
    self.sup1 = int(blockline[3])
    self.strand1 = int(blockline[4])
    self.c2 = blockline[6]
    self.inf2 = int(blockline[7])
    self.sup2 = int(blockline[8])
    self.strand2 = int(blockline[9])

  def innitializesup(self, name, bkp):
    """
    A breakpoint object bkp read in from the file Nonrefinedbreakpoints.txt produced by CASSIS 
    is used to initialize the sup end of the block in genome1 and it's corresponding end in genome2.
    """

    self.id = name
    self.c1 = bkp.sRchr
    self.strand2 = findstrand(bkp.sRstrandA, bkp.sOstrandA)
    self.c2 = bkp.sOchrA
    self.sup1 = bkp.sRinf

    if self.strand2 == 1:
      self.sup2 = bkp.sOinfA
      self.right = bkp.sOsupA
    else: 
      self.inf2 = bkp.sOsupA
      self.left = bkp.sOinfA

  def innitializeinf(self, name, bkp):
    self.id = name
    self.c1 = bkp.sRchr
    self.strand2 = findstrand(bkp.sRstrandB, bkp.sOstrandB)
    self.c2 = bkp.sOchrB 
    self.inf1 = bkp.sRsup 

    if self.strand2 == 1:
      self.inf2 = bkp.sOsupB
      self.left = bkp.sOinfB
    else:
      self.sup2 = bkp.sOinfB
      self.right = bkp.sOsupB
#        _____________________________
#_______/    Refinement               \________________________________________________

  def refinesup(self, rbkp):
    """
    Refined breakpoint objects read in from segmentation.txt provided by cassis 
    are used to refine the blocks.
    """
  
    if rbkp.status == 1:
      supdiff = rbkp.newBegin - self.sup1
      self.sup1 = rbkp.newBegin
      if self.strand2 == 1:
        self.sup2 = self.sup2 + supdiff
      else: 
        self.inf2 = self.inf2 - supdiff

  def refineinf(self, rbkp):
    if rbkp.status == 1:
      infdiff = self.inf1 - rbkp.newEnd
      self.inf1 = rbkp.newEnd
      if self.strand2 == 1:
        self.inf2 = self.inf2 - infdiff
      else: 
        self.sup2 = self.sup2 + infdiff 
#        ______________________
#_______/    I/O               \________________________________________________
 
  def printblock(self):
    print(self.id, self.c1, self.inf1, self.sup1, self.strand1, self.id, self.c2, self.inf2, self.sup2, self.strand2)

  def writeblock(self):
    return [self.id, self.c1, self.inf1, self.sup1, self.strand1, self.id, self.c2, self.inf2, self.sup2, self.strand2]

  def writeblockforcassis(self):
    return [self.id, self.c1, self.inf1, self.sup1, self.c2, self.inf2, self.sup2, self.strand2]
#        _________________________________
#_______/    Modify a block               \________________________________________________
 
  def swapgenomes(self):
    self.c1, self.c2 =  self.c2, self.c1
    self.inf1, self.inf2 =  self.inf2, self.inf1
    self.sup1, self.sup2 =  self.sup2, self.sup1
    
  def intersect(self, b2):
    self.inf1 = max(self.inf1, b2.inf1)
    self.inf2 = max(self.inf2, b2.inf2)
    self.sup1 = min(self.sup1, b2.sup1)
    self.sup2 = min(self.sup2, b2.sup2)
#        _____________________________________
#_______/    Info about a block               \________________________________________________
 
  def negative(self):
    if self.sup1 < self.inf1 or self.sup2 < self.inf2:
      return True
    else:
      return False
#        __________________________________________________
#_______/    Compare positions of two blocks               \________________________________________________

  def emptyintersection(self, b2):
    if self.c1 == b2.c1 and self.c2 == b2.c2:
      if self.inf1 > b2.sup1 or self.inf2 > b2.sup2 or self.sup1 < b2.inf1 or self.sup2 < b2.inf2:
        return True
      else: 
        return False
    else:
      return True

  def overlap1(self, b2, show = False):
    if self.c1 != b2.c1:
      return False
    if self.inf1<=b2.inf1<=self.sup1 or self.inf1<=b2.sup1<=self.sup1:
      if show:
        print("overlap in g1 of", self.id, (self.inf1, self.sup1), "with", b2.id, (b2.inf1, b2.sup1)) 
      return True
    else: 
      return False       
           
  def overlap2(self, b2, show = False):
    if self.c2 != b2.c2:
      return False
    if self.inf2<=b2.inf2<=self.sup2 or self.inf2<=b2.sup2<=self.sup2:
      if show:
        print("overlap in g2 of", self.id, (self.inf2, self.sup2), "with", b2.id, (b2.inf2, b2.sup2)) 
      return True
    else: 
      return False       

  def contains1(self, b2, show = False):
    """
    Tests if self contains b2 in genome1

    """
    if self.c1 != b2.c1:
      return False
    if self.inf1<=b2.inf1<=b2.sup1<=self.sup1:
      if show:
        print("contained in g1", b2.id, (b2.inf1, b2.sup1), "in", self.id, (self.inf1, self.sup1)) 
      return True
    else:
      return False 

  def contains2(self, b2, show = False):
    """
    Tests if self contains b2 in genome2
    """
 
    if self.c2 != b2.c2:
      return False
    if self.inf2<=b2.inf2<=b2.sup2<=self.sup2:
      if show:
        print("contained in g2", b2.id, (b2.inf1, b2.sup1), "in", self.id, (self.inf1, self.sup1)) 
      return True
    else:
      return False 

################################################################################
#        ______________________
#_______/    I/O               \________________________________________________
 
def output(idTOblock):
  c1TOblocks = blocksingenome1(idTOblock)
  chromosomes = listofchromosomes(c1TOblocks)
  for c1 in chromosomes:
    c1TOblocks[c1].sort(key=lambda x: x.inf1)
    for block in c1TOblocks[c1]:
      block.printblock()

def printblocks(idTOblock, name):
  output = []  
  c1TOblocks = blocksingenome1(idTOblock)
  chromosomes = listofchromosomes(c1TOblocks)
  for c1 in chromosomes:
    for block in c1TOblocks[c1]:
      output.append(block.writeblock())

  with open('{}.tsv'.format(name), 'w') as f:
    writer = csv.writer(f, delimiter='\t')
    writer.writerows(output)

def printblocksforcassis(idTOblock, name):
  output = []  
  c1TOblocks = blocksingenome1(idTOblock)
  chromosomes = listofchromosomes(c1TOblocks)
  for c1 in chromosomes:
    for block in c1TOblocks[c1]:
      output.append(block.writeblockforcassis())

  with open('{}.tsv'.format(name), 'w') as f:
    writer = csv.writer(f, delimiter='\t')
    writer.writerows(output)

def readblocksfromortho(orthofile):
  idTOblock = defaultdict(dict)  
  f = open(orthofile, "r")
  for blockline in f:
    blockline = re.split(r'\t', blockline[:-1])
    block = Block()
    block.innitializewithblockline(blockline)
    idTOblock[block.id] = block
  return idTOblock

#        _________________________________
#_______/    Info on blocks               \_____________________________________

def breakpoints(idTOblock):
  """
  Provides information on breakpoints
  """
  c1TOblocks = blocksingenome1(idTOblock)
  breakpoints1 = []
  for c1 in c1TOblocks:
    if len(c1TOblocks[c1])>1:
      breakpoints1.append(c1TOblocks[c1][0].inf1)
      for i in range(1, len(c1TOblocks[c1])):
        if c1TOblocks[c1][i].inf1 > c1TOblocks[c1][i-1].sup1:
          breakpoints1.append(c1TOblocks[c1][i].inf1 - c1TOblocks[c1][i-1].sup1)
          print(c1TOblocks[c1][i].inf1,c1TOblocks[c1][i-1].sup1,c1TOblocks[c1][i].inf1 - c1TOblocks[c1][i-1].sup1, c1)

  c2TOblocks = blocksingenome2(idTOblock)
  breakpoints2 = []
  for c2 in c2TOblocks:
    if len(c2TOblocks[c2]) > 1:
      breakpoints2.append(c2TOblocks[c2][0].inf2)
      for i in range(1, len(c2TOblocks[c2])):
        if c2TOblocks[c2][i].inf2 > c2TOblocks[c2][i-1].sup2:
          breakpoints2.append(c2TOblocks[c2][i].inf2 - c2TOblocks[c2][i-1].sup2)

  print("Length", sum(breakpoints1), sum(breakpoints2))
  print("Median", np.median(breakpoints1), np.median(breakpoints2))
  print("Max", max(breakpoints1), max(breakpoints2))
  print("Number", len(breakpoints1), len(breakpoints2))

def blocklength(idTOblocks):
  """
  Sum of the lengths of the blocks is comptued with overlaps counted once.

  """


  lengths1 = []
  lengths2 = []  
  for name in idTOblocks:
    lengths1.append(max(0,idTOblocks[name].sup1 - idTOblocks[name].inf1))  
    lengths2.append(max(0,idTOblocks[name].sup2 - idTOblocks[name].inf2)) 

  c1TOblocks = blocksingenome1(idTOblocks)
  lengths3 = [] 
  for c1 in c1TOblocks:
    #we check the blocks of c1
    i = 0
    while i < len(c1TOblocks[c1]):
      #inf of a block
      inf = c1TOblocks[c1][i].inf1
      goon = True 
      while goon:
        #check if the next block exists
        if i+1 < len(c1TOblocks[c1]):
          #check if it overlaps the previous one, if so continue
          if c1TOblocks[c1][i+1].inf1 < c1TOblocks[c1][i].sup1:
            i+=1
          else:
            goon = False
        else:
          goon = False       
      sup = c1TOblocks[c1][i].sup1
      lengths3.append(sup - inf)
      i+=1

  lengths4 = []
  c2TOblocks = blocksingenome2(idTOblocks)
  for c2 in c2TOblocks:
    #we check the blocks of c1
    i = 0
    while i < len(c2TOblocks[c2]):
      #inf of a block
      inf = c2TOblocks[c2][i].inf2
      goon = True 
      while goon:
        #check if the next block exists
        if i+1 < len(c2TOblocks[c2]):
          #check if it overlaps the previous one, if so continue
          if c2TOblocks[c2][i+1].inf2 < c2TOblocks[c2][i].sup2:
            i+=1
          else:
            goon = False
        else:
          goon = False       
      sup = c2TOblocks[c2][i].sup2
      lengths4.append(sup - inf)
      i+=1
  
  print("\nInfo on block lengths:")
  print("Total length of blocks", sum(lengths3), sum(lengths4))
  print("Proportion of human genome (if the first genome is human)", 100*sum(lengths3)/3096649726)
  print("Median length", np.median(lengths1), np.median(lengths2))
  print("Max length", max(lengths1), max(lengths2))
  print("Min length", min(lengths1), min(lengths2))
  print("Number of blocks", len(lengths1), len(lengths2),"\n")

#        _________________________________
#_______/   Transformations               \________________________________________________
 
def blocksingenome1(idTOblock):
  """
  Orders blocks on the chromosomes of genome1.

  @note: all the inf and sup positions of the blocks are supposed to be known 
         and the blocks are supposed to be non nested.
  """

  c1TOblocks = defaultdict(dict)

  for name in idTOblock:
    c1 = idTOblock[name].c1
    if c1 not in c1TOblocks:
      c1TOblocks[c1] = []
    c1TOblocks[c1].append(idTOblock[name]) 

  for c1 in c1TOblocks:
    c1TOblocks[c1].sort(key=lambda x: x.inf1)

  return c1TOblocks

def blocksingenome2(idTOblock):
  """
  Orders blocks on the chromosomes of genome2.

  @note: blocks are supposed to be non nested, but one of the positions of 
         of a block might be "None" 

  """

  c2TOblocks = defaultdict(dict)
  for name in idTOblock:
    c2 = idTOblock[name].c2
    if c2 not in c2TOblocks:
      c2TOblocks[c2] = []
    c2TOblocks[c2].append(idTOblock[name]) 

  for c2 in c2TOblocks:
    c2TOblocks[c2].sort()

  return c2TOblocks

def positionsingenome1(idTOblock):
  c1TOextTOblock = defaultdict(dict)
  for name in idTOblock:
    block = idTOblock[name]
    c1TOextTOblock[block.c1][block.inf1] = block
    c1TOextTOblock[block.c1][block.sup1] = block
  return c1TOextTOblock       
#        _________________________
#_______/   Overlap               \________________________________________________
 
def blocksoverlapingenome1(idTOblock, show = False):
  overlap = set()
  nested = set()
  c1TOblocks = blocksingenome1(idTOblock)
  
  for c1 in c1TOblocks:
    blocks = c1TOblocks[c1]
    for i in range(1,len(blocks)):
      if blocks[i-1].overlap1(blocks[i],show):
        overlap.add(blocks[i-1].id)
        if blocks[i-1].contains1(blocks[i],show) or blocks[i].contains1(blocks[i-1],show):
          nested.add(blocks[i-1].id)
  return overlap, nested 

def blocksoverlapingenome2(idTOblock, show = False):
  overlap = set()
  nested = set()
  c2TOblocks = blocksingenome2(idTOblock)
  
  for c2 in c2TOblocks:
    blocks = c2TOblocks[c2]
    for i in range(1,len(blocks)):
      if blocks[i-1].overlap2(blocks[i],show):
        overlap.add(blocks[i-1].id)
        if blocks[i-1].contains2(blocks[i],show) or blocks[i].contains2(blocks[i-1],show):
          nested.add(blocks[i-1].id)

  return overlap, nested 

def negativeblocks(idTOblock):
  negative = set()
  for name in idTOblock:
    if idTOblock[name].negative():
      negative.add(name)
  return negative

def removenestedblocks(idTOblock, show = False):
  names = set()
  for name in idTOblock:
    names.add(name)
  
  contained = set()
  for (id1, id2) in itertools.combinations(names, 2):
    b1, b2 = idTOblock[id1], idTOblock[id2]
    if b1.contains1(b2) or b1.contains2(b2):
      contained.add(b2.id)  
    if b2.contains1(b1) or b2.contains2(b1):
      contained.add(b1.id) 

  for name in contained:
    if show:
      print("Block {} is contained in some other block in one of the genomes".format(name))
      idTOblock[name].printblock()
    del idTOblock[name]

#        ________________________________________
#_______/    Homogenize the blocks               \________________________________________________

def homogenizedidsingenome1(idTOblock):
  """
  Orders blocks on the chromosomes of genome1 and provides them with a naming respecting this ordering.
  """
  idTOhomogenizedid = defaultdict(dict)
  c1TOblocks = blocksingenome1(idTOblock)
  
  index = 1
  chromosomes = listofchromosomes(c1TOblocks)
  for c1 in chromosomes:
    for block in c1TOblocks[c1]:
      idTOhomogenizedid[block.id] = index
      index+=1
  return idTOhomogenizedid

def renameblocks(idTOblock, idTOnewid):
  newidTOblock = defaultdict(dict)
  for name in idTOblock:
    block = copy.deepcopy(idTOblock[name])
    block.id = idTOnewid[name]
    newidTOblock[block.id] = block
  return newidTOblock

def swapgenomes(idTOblock):
  for name in idTOblock:
    idTOblock[name].swapgenomes()
  return idTOblock

def compareblockorder(idTOblock1, idTOblock2):
  """
  For two sets of blocks with a homogenized naming checks if the order of the blocks on the chromosomes of genome2 is the same. 
  """
  c2TOblocks1 = blocksingenome2(idTOblock1)
  c2TOblocks2 = blocksingenome2(idTOblock2)
  for c2 in c2TOblocks1:
    if c2 in c2TOblocks2:
      if len(c2TOblocks1[c2]) == len(c2TOblocks2[c2]):
        for i in range(0,len(c2TOblocks1[c2])):
          if c2TOblocks1[c2][i].id != c2TOblocks2[c2][i].id:
            return False
      else:
        return False
    else: 
      return False

  return True  

#        _______________________________________________
#_______/    Intersect two sets of blocks               \________________________________________________

def emptyintersection(idTOblock1, idTOblock2):
  emptyintersection = set()
  for name in idTOblock1:
    if idTOblock1[name].emptyintersection(idTOblock2[name]):
      emptyintersection.add(name)
  return emptyintersection

def intersectblocks(idTOblock1, idTOblock2):
  idTOblock = defaultdict(dict)
  for name in idTOblock1:
    block = copy.deepcopy(idTOblock1[name])
    block.intersect(idTOblock2[name])
    idTOblock[name] = block
  return idTOblock

#        ___________________________________________
#_______/    Merge consecutive blocks               \_________________________________

def consecutiveingenome2(b1, b2, idTOblock):
  """
  Tests if block1 and block2 are consecutive in genome2,
  meaning that they lie on the same chromosome, 
  there are no blocks in between them and 
  their strandedness agrees.

  """
  if b1.c2 == b2.c2:
    if b1.inf2 < b2.inf2 and b1.strand2 == b1.strand1 and b2.strand2 == b2.strand1:
      first, second = b1, b2
    elif b1.inf2 > b2.inf2 and b1.strand2 != b1.strand1 and b2.strand2 != b2.strand1:
      first, second = b2, b1
    else:
      return False
  else:
    return False   
 
  c2TOblocks = blocksingenome2(idTOblock)
  blocks = c2TOblocks[b1.c2] 
  for i in range(1,len(blocks)):
    if blocks[i-1].id == first.id:
      if blocks[i].id == second.id:
        return True
      else:
        return False
  
def mergetwoconsecutiveblocks(block1, block2):
  """
  Merges two blocks.
  """
  block = copy.deepcopy(block1)
  block.id = str(block1.id)+'+'+str(block2.id)
  block.strand2 = block1.strand1 * block1.strand2
  block.inf1 = min(block1.inf1, block2.inf1)
  block.sup1 = max(block1.sup1, block2.sup1)
  block.inf2 = min(block1.inf2, block2.inf2)
  block.sup2 = max(block1.sup2, block2.sup2)
  return block

def mergeconsecutive(idTOblock):
  """
  Merges consecutive blocks in a set of syntenic blocks.
  """
  c1TOblocks = blocksingenome1(idTOblock)
  for c1 in c1TOblocks:
    for i in range(1,len(c1TOblocks[c1])):
      if consecutiveingenome2(c1TOblocks[c1][i-1], c1TOblocks[c1][i], idTOblock):
        mergedblock = mergetwoconsecutiveblocks(c1TOblocks[c1][i-1], c1TOblocks[c1][i])
        del idTOblock[c1TOblocks[c1][i-1].id]
        del idTOblock[c1TOblocks[c1][i].id]
        c1TOblocks[c1][i] = mergedblock
        idTOblock[mergedblock.id] = mergedblock
#        ____________________________
#_______/    Auxiliary               \________________________________________________

def listofchromosomes(cTOsmth):
  """
  This function is used to ensure that the order of the chromosomes is always the same.
  This is important for the naming of blocks and comparing the blocks by eye once they are printed out. 
  """
  chromosomes = []
  for c in cTOsmth:
    chromosomes.append(c)
  chromosomes.sort()
  return chromosomes 

def findstrand(strand1, strand2):
  if strand1 == strand2:
    return 1
  else: 
    return -1 
