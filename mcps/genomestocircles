#!/usr/bin/env python3
# Pijus Simonaitis                                               Winter 2018
#
"""
Reads a pair of genomes.
Makes breakpoint graph.
Extracts circles from a breakpoint graph.
Plots circles in OUT_GRAPHS.

"""

import argparse
import pickle
import os
import numpy as np
import itertools

import networkx as nx

from collections import defaultdict
from lib import genome, breakpointgraph, evenpaths, mcps

#        ______________________
#_______/    Do Everything     \________________________________________________


def main():
  desc = 'Reads a pair of genomes, makes breakpoint graph,'+\
         'extracts circles and computes MCPS. Cost function must be chosen by hand.' 
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('GENOMEPAIR',
                      help='a pickle containing a pair of genomes pickled by picklegenomepair.')
  parser.add_argument('NAME',
                      help='name for breakpoint graphs.')

  args = parser.parse_args()
  genomesfile = args.GENOMEPAIR
  name = args.NAME


  print("Constructing breakpoint graph")

  (g1,g2) = pickle.load(open(genomesfile,"rb"))
  bg = breakpointgraph.genomesTObreakpoinggraph(g1, g2) 
  nxcomponents = breakpointgraph.breakpointgraphTOcomponents(bg)


  (aapaths, bbpaths) = breakpointgraph.componentsTOevenpaths(nxcomponents)
  nxcircles = breakpointgraph.breakpointgraphTOnxcircles(bg)


  #breakpointgraph.plotgraphs(nxcomponents, name)
  #breakpointgraph.plotgraphs(aapaths, name)
  #breakpointgraph.plotgraphs(bbpaths, name)
 
  print("Analysing breakpoint graph")

  max_a = 0
  for i in aapaths:
    if len(i.nodes()) > max_a:
      max_a = len(i.nodes())  
 
  max_b = 0
  for i in bbpaths:
    if len(i.nodes()) > max_b:
      max_b = len(i.nodes())  

  max_c = 0
  length4 = []
  for i in nxcircles:
    if len(i.nodes()) == 4:
      length4.append(i)
    if len(i.nodes()) > max_c:
      max_c = len(i.nodes())
  
  length = 0
  for i in aapaths + bbpaths + nxcircles:
    length+=(len(i.nodes())/2 - 1)   
  print("Scenario length:", length) 
  print("number of AA paths, BB paths and circles: {}, {}, {}".format(len(aapaths), len(bbpaths), len(nxcircles)))
  print("max AA, max BB, max circle: {}, {}, {}".format(max_a, max_b, max_c))
  print("% of sure 2-breaks:", len(length4)/length, "\n")

  print("Computing costs for the circles of a breakpoint graph")
  circles_cost = 0 
  for nxc in nxcircles:
    circle = breakpointgraph.nxcircleTOcircle(nxc)
    circles_cost += mcps.parsimoniousMCPS(circle)

  AApathTOBBpathTOcircles  = breakpointgraph.evenpathsTOnxcircles(aapaths, bbpaths)
  paths_cost = evenpaths.mcpsofevenpaths(AApathTOBBpathTOcircles, g1, g2)
  

  print("MCPS costs of circles, MCPS costs of even paths, Total MCPS cost: {}, {}, {}".format(-1*circles_cost, -1*paths_cost, -1*(circles_cost+paths_cost)))


if __name__ == "__main__":
  main()
