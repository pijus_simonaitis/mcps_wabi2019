#!/usr/bin/env python
# Pijus Simonaitis                                               Winter 2018
#
"""
Library for Genome object

"""

from collections import defaultdict


class Genome(object):
  """
  An object storing the extremities of syntenic blocks togehter with their physical positions
  and (optional) Hi-C heatmap values. 

  """

  def __init__(self, name, extremities, heat = None):
    """
    
    Physical positions of the extremities of the syntenic blocks
    
    """
    self.name = name
    self.chrTOextTOpos = defaultdict(dict)
    self.extTOchrpos = defaultdict(dict)
    self.chrTOext = defaultdict(dict)
    self.extTOextTOheat = heat 
    self._innitialize(extremities)

  def _innitialize(self, extremities):

    for (extremity, chromosome, position) in extremities:
      self.chrTOextTOpos[chromosome][extremity] = position
      self.extTOchrpos[extremity] = (chromosome, position)

    for chromosome in self.chrTOextTOpos:
      extremities = list()
      for extremity in self.chrTOextTOpos[chromosome]:
        extremities.append((extremity, self.chrTOextTOpos[chromosome][extremity]))
      extremities.sort(key=lambda x:x[1])
      
      self.chrTOext[chromosome] = list() 
      for extremity in extremities:
        self.chrTOext[chromosome].append(extremity[0]) 



  def blockanalysis(self):
    """
    Lists the lengths of intergenic regions and blocks. 
    Here I do not known the lengths of the chromosomes, thus the telomeric regions are missing. 

    """
    
    inter = list()
    block = list()
    for chro in self.chrTOext:
      inter.append(self.extTOchrpos[self.chrTOext[chro][0]][1])
      for i in xrange(len(self.chrTOext[chro])-1):
        if i%2 == 0:
          block.append(self.extTOchrpos[self.chrTOext[chro][i+1]][1]-self.extTOchrpos[self.chrTOext[chro][i]][1])
        else: 
          inter.append(self.extTOchrpos[self.chrTOext[chro][i+1]][1]-self.extTOchrpos[self.chrTOext[chro][i]][1])
