#!/usr/bin/env python3
# Pijus Simonaitis                                               Winter 2018
#
"""
Library for breakpoint graph.

Breakpoint graph is a networkx multigrpah with edges having colors black and gray.
It's connected components are either circles, odd paths or even paths.

The endponts of the odd paths are merged to form a circle.
Resulting telomer is named "t1"

The even paths are either completed with an edge to form a circle,
or two even paths are merged to form a circle.
Resulting telomers are named "t1" and "t2"  

I distinguish between nxcircles that are netwrokx graphs and 
circles, that are the lists of vertices.

In the latter case I take a convention that for a circle [v_{0},v_{1},\ldots, v_{n}], 
the edge (v_{0},v_{1}) is black. 

"""

import sys
import networkx as nx
import random 
import os
import itertools
import copy

from collections import defaultdict
from . import twobreaks, mcps

OUT_DIR = "data/breakpointgraphs"

def genomesTObreakpoinggraph(g1, g2):
  """
  Generates a breakpoint graph of genomes g1 and g2 with a separate vertex for every telomere.
  
  This means that every even path, odd path and circle of a breakpoint graph is a separate
  connected component.

  Vertices of a breakpoint graph have format (ext, c1, c2), where ci is the chromosome
  containing ext in gi.
  
  ext is of a format (block ID, extremity (either 0 or 1))

  """
 
  bg=nx.MultiGraph()
  
  #Black edges are the adjacencies of g1
  for c1 in g1.chrTOext:
    #Telomeric adjacencies
    chrom1 = g1.chrTOext[c1]
    ext = chrom1[0]
    c2 = g2.extTOchrpos[ext][0]
    bg.add_edge('t{}1b'.format(c1),(ext, c1, c2), color='black')

    ext = chrom1[-1]
    c2 = g2.extTOchrpos[ext][0]
    bg.add_edge('t{}2b'.format(c1),(ext, c1, c2), color='black')
   
    #Internal adjacencies 
    for i in range(1,len(chrom1)-1,2):
      ext1 = chrom1[i]
      ext2 = chrom1[i+1]
      c21 = g2.extTOchrpos[ext1][0]
      c22 = g2.extTOchrpos[ext2][0]
      bg.add_edge((ext1, c1, c21),(ext2, c1, c22), color='black')    

  #Gray edges are the adjacencies of g2
  for c2 in g2.chrTOext:
    #Telomeric adjacencies
    chrom2 = g2.chrTOext[c2]
    ext = chrom2[0]
    c1 = g1.extTOchrpos[ext][0]
    bg.add_edge('t{}1g'.format(c2),(ext, c1, c2), color='gray')

    ext = chrom2[-1]
    c1 = g1.extTOchrpos[ext][0]
    bg.add_edge('t{}2g'.format(c2),(ext, c1, c2), color='gray')

    #Internal adjacencies 
    for i in range(1,len(chrom2)-1,2):
      ext1 = chrom2[i]
      ext2 = chrom2[i+1]
      c11 = g1.extTOchrpos[ext1][0]
      c12 = g1.extTOchrpos[ext2][0]
      bg.add_edge((ext1, c11, c2),(ext2, c12, c2), color='gray')

  return bg


def breakpointgraphTOcomponents(bg):
  """
  Transforms a breakpoint graph into a list of connected components.

  """
  nxcomponents = list()
  for i in nx.connected_components(bg):
    H = nx.MultiGraph(bg.subgraph(i))
    nxcomponents.append(H)
  return nxcomponents


def plotgraphs(graphs, name):
  """
  Plots graphs from a given list in OUT_DIR

  """

  if not os.path.exists(OUT_DIR):
    os.makedirs(OUT_DIR)  

  index = 0
  for G in graphs:
    graph = nx.nx_pydot.to_pydot(G)
    graph.write_png('{}/{}-{}.png'.format(OUT_DIR,name,str(index)))
    index +=1

#        _________________________
#_______/       Auxilliary        \_____________________________________________


def degreeonevertices(G):
  """
  Outputs a list of degree 1 vertices of G.
  """
  ends = []
  for u in G.nodes(): 
    if G.degree(u) == 1:
      ends.append(u)
  return ends 

#        _____________________________________
#_______/       Even paths to nxcircles        \_____________________________________________

def componentsTOevenpaths(components):
  """
  Given connected components of a breakpoint graph 
  outputs AA and BB paths.

  """
  aapaths, bbpaths = list(), list()
  for component in components: 
    if len(component.nodes())%2 == 0:
       ends = degreeonevertices(component)
       if len(ends) == 2: 
         #take one edge incident to an end of a path 
         [u,v] = list(component.edges(ends[0]))[0]
         if component[u][v][0]['color'] == 'black':
           aapaths.append(component)
         elif component[u][v][0]['color'] == 'gray':
           bbpaths.append(component)
         else:
           sys.exit('Color must be black or gray')
       if len(ends) > 2:
         sys.exit('One of the components is neither a path nor a circle')
  return (aapaths, bbpaths)

def pairofevenpathsTOnxcircles(path1, path2):
  """
  Given a pair of an AA path and a BB paths,
  outputs the two circles obtained after merging these paths.

  """
  ends1, ends2 = degreeonevertices(path1), degreeonevertices(path2)
  
  #compose both paths into a single graph
  #the nodes are uniquely named, thus the new graph has two
  #connected components 
  composed_paths = nx.compose(path1, path2)

  #contract the telomers to obtain the two possible circles 
  c1 = copy.deepcopy(composed_paths)
  c1 = nx.contracted_nodes(c1, ends1[0], ends2[1], self_loops=True)
  c1 = nx.contracted_nodes(c1, ends1[1], ends2[0], self_loops=True)
 
  c2 = copy.deepcopy(composed_paths)
  c2 = nx.contracted_nodes(c2, ends1[0], ends2[0], self_loops=True)
  c2 = nx.contracted_nodes(c2, ends1[1], ends2[1], self_loops=True)

  #rename the telomers
  mapping = {ends1[0]: 't1', ends1[1]: 't2'}
  c1 = nx.relabel_nodes(c1, mapping, copy=False)
  c2 = nx.relabel_nodes(c2, mapping, copy=False)

  return [c1, c2]

def evenpathTOnxcircle(path, col):
  """
  Adds an edge of color col between the ends of an even path,
  renames the ends and outputs the circle.

  """
  ends = degreeonevertices(path)
  
  circle = copy.deepcopy(path)
  circle.add_edge(ends[0],ends[1], color = col)
  mapping = {ends[0]: 't1', ends[1]: 't2'}
  circle = nx.relabel_nodes(circle, mapping, copy=False)

  return circle
       
def evenpathsTOnxcircles(aapaths, bbpaths):
  """
  Given AA paths and BB paths returns 
  a dictionary storing the pairs of circles obtained merging AA and BB paths. 

  'empty_a' and 'empty_b' denotes a path of length 1 joining two telomers.  
  """

  AApathTOBBpathTOnxcircles = defaultdict(dict)
 
  for A in aapaths:
    AApathTOBBpathTOnxcircles[A]['empty_b'] = [evenpathTOnxcircle(A, 'gray')]
  for B in bbpaths:
    AApathTOBBpathTOnxcircles['empty_a'][B] = [evenpathTOnxcircle(B, 'black')]
  
  for A in aapaths:
    for B in bbpaths:
      AApathTOBBpathTOnxcircles[A][B] = pairofevenpathsTOnxcircles(A, B) 
  
  return AApathTOBBpathTOnxcircles 
   

def plotevenpathnxcircles(AApathTOBBpathTOnxcircles,name):
  nxcircles = list()
  for A in AApathTOBBpathTOcircles:
    for B in AApathTOBBpathTOcircles[A]:
      nxcircles += AApathTOBBpathTOcircles[A][B]
  plotgraphs(nxcircles, name+'_even_paths')

#        ___________________________________________________
#_______/       odd paths and nxcircles to nxcircles        

def breakpointgraphTOnxcircles(bg):
  """
  Transforms a breakpoint graph into a list of circles that are still networx graphs.
  The ends of an odd path are contracted to form a circle.

  """
  nxcircles = list()
  for i in nx.connected_components(bg):
    H = nx.MultiGraph(bg.subgraph(i))
    #odd paths having more than 2 edges
    if len(H.nodes())%2 == 1 and len(H.nodes())>3:
      #Transform an odd path into a circles
      #by contracting its ends and renaming them by 't'
      ends = list()
      for u in H.nodes(): 
        if H.degree(u) == 1:
          ends.append(u)
      H = nx.contracted_nodes(H, ends[0], ends[1], self_loops=True)
      mapping = {ends[0]: 't1'}
      H = nx.relabel_nodes(H, mapping, copy=False)
      nxcircles.append(H)
    #circles having more than 2 edges
    elif len(degreeonevertices(H)) == 0 and len(H.nodes)>2:
      nxcircles.append(H) 
  return nxcircles

#        __________________________________
#_______/       nxgraphs to circles        \_____________________________________________


def nxcircleTOcircle(nxcircle):
  """
  Returns a list of nodes respecting their order on the circle and the first edge being black.

  """

  #finds a gray edge 
  edge = None
  for i in nxcircle.edges():
    if (nxcircle[i[0]][i[1]][0]['color'] == 'gray' and edge == None):
      edge = i

  #removes a chosen gray edge
  nxcircle.remove_edge(edge[0], edge[1], 0)

  #finds a path joining the ends of the gray edge
  circle = nx.shortest_path(nxcircle, source=edge[0], target=edge[1], weight=None)

  return circle

def genomesTOcircles(genomesfile, show = False):
  #loads a pair of genomes
  (g1,g2) = pickle.load(open(genomesfile,"rb"))
  #constructs a breakpoint graph
  bg = genomesTObreakpoinggraph(g1, g2)
  #extracts the connected components of bg
  nxcircles = breakpointgraphTOnxcircles(bg)
  #plots the circles in OUT_GRAPHS
  if show:
    plotgraphs(nxcircles, g1.name+'-'+g2.name)
    print("Finished plotting the graphs.")

  #transforms networkx graphs of circles into the lists of vertices

  circles = []
  for nxcircle in nxcircles:
    circle = nxcircleTOcircle(nxcircle)
    if circle != False:
      circles.append(circle)
      
  return circles 

#        _________________________________
#_______/       Randomized circles        \_____________________________________________



def invertcircle(circle, a, b):
  """
  Cuts circle before a and before b and inverts the segment in between
  """
  circle[a:b] = reversed(circle[a:b]) 
  return circle  

def randomizecircle(circle, k):
  """
  Performs k random 2-breaks on the gray edges of a circle.
  Every 2-break keeps the circle connected.
  """

  group = list()
  for i in range(len(circle)/2):
    group.append(i)

  for i in range(k):
    #get random cut places a, b with both of them being even and a<b
    rand = random.sample(group, 2)
    if rand[0]>rand[1]:
      temp = rand[0]
      rand[0] = rand[1]
      rand[1] = temp  
    a, b = 2*rand[0]+2, 2*rand[1]+2

    circle = invertcircle(circle, a, b)
  return circle


  
#        _________________________________________________________
#_______/       Randomized parsimonio scenario on a circle        \_____________________________________________

def randomscenario(cir):
  """
  Outputs a random parsimonious scenario for a circle

  """

  #If circle length is 2, then no moves are required
  if len(cir) == 2:
    return []
  
  #Choose randomly two even indexes a<b after which the circle will be cut
  [a,b] = sorted(random.sample(range(0, len(cir), 2), 2))

  #This is a 2-break that is performed
  tb = [[cir[a],cir[a+1]],[cir[b],cir[b+1]],[cir[a],cir[b+1]],[cir[a+1],cir[b]]]

  #Transfrom the 2-break into canonical form
  move = twobreaks.canonicaltb(tb)

  #If circle length is 4, then one move suffice 
  if len(cir) == 4:
    return [move]
  else: 
    moves1 = randomscenario(cir[:a+1]+cir[b+1:])
    moves2 = randomscenario([cir[b]]+cir[a+1:b])

  return [move]+moves1+moves2 

def costofascenario(moves, cost):
  addedcosts = 0
  for move in moves:
    addedcosts += cost[move]
  return addedcosts


def manyscenarios(cir, it, cost):
  multiplescenarios = []
  for i in range(it):
    scenario = randomscenario(cir)
    scenariocost = costofascenario(scenario, cost)
    multiplescenarios.append(scenariocost) 
  return multiplescenarios
  

#        ___________________________________________
#_______/       2-break on a brakpoint graph        \_____________________________________________


def mergecircles(circ1,circ2,a,b):
  return [circ1[:a+1]+circ2[b+1:]+circ2[:b+1]+circ1[a+1:],circ1[:a+1]+circ2[b::-1]+circ2[:b:-1]+circ1[a+1:]]

def splitcircle(circ,a,b):
  return [circ[:a+1]+circ[b+1:],[circ[b]]+circ[a+1:b]]

def mixcircle(circ,a,b):
  return circ[:a+1]+circ[b:a:-1]+circ[b+1:]

def generatepairs(length, color = "black"):
  if color == "black":
    breaklist = [i for i in range(0,length,2)]
  elif color == "gray":
    breaklist = [i for i in range(1,length,2)]
  else:
    sys.exit('Color must be black or gray')
  for (a,b) in itertools.combinations(breaklist, 2):
    yield (a,b) 


def generateelement(length, color = "black"):
  if color == "black":
    breaklist = [i for i in range(0,length,2)]
  elif color == "gray":
    breaklist = [i for i in range(1,length,2)]
  else:
    sys.exit('Color must be black or gray')
  for a in breaklist:
    yield a
