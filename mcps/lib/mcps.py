#!/usr/bin/env python3
# Pijus Simonaitis                                               Winter 2018
#
"""
Library for solving Minimum Cost Parsimonious Scenario

"""
import numpy as np
from . import twobreaks
#        _________________________
#_______/        Functions        \_____________________________________________


def MCPS(circle, cost, maximize = False):
  """
  Computes the cost of a MCPS for a circle that is a list of vertices
  with a first edge being black.

  MCPS[a][b] for a<b  stores the MCPS cost of a sub-circle (circle[a],\ldots,circle[b])

  border is a difference b-a, border must be odd to obtain a valid alternating circle.
 
  """

  size = len(circle)

  #beginning of the initialization
  MCPS = list()
  for i in range(size):
    MCPS.append([])
    for j in range(size):
      if not maximize:
        MCPS[i].append(np.inf)
      if maximize:
        MCPS[i].append(-np.inf)

  for i in range(size-1):
    MCPS[i][i+1] = 0  

  #end of the initialization

  for border in range(3, size, 2):
    #for every pair (a,b) with b-a = border
    for a in range(size-border):
      b = a + border
      #for every pair i<j in between a and b such that i-a, j-i and b-j are all odd
      for i in range(a+1,b,2):
        for j in range(i+1, b, 2):
          #identify a 2-break acting on these vertices
          tb = twobreakoncircle((a,i,j,b),circle)
          #update MCPS
          if tb in cost:
            new_cost = MCPS[a][i] + MCPS[i][j] + MCPS[j][b] + cost[tb]
            if maximize == False and new_cost < MCPS[a][b]:
              MCPS[a][b] = new_cost     
            if maximize and new_cost > MCPS[a][b]:
              MCPS[a][b] = new_cost 

  return MCPS[0][size-1] 


def parsimoniousMCPS(circle):
  """
  Computes the cost of a MCPS for a circle that is a list of vertices
  with a first edge being black.

  MCPS[a][b] for a<b  stores the MCPS cost of a sub-circle (circle[a],\ldots,circle[b])

  border is a difference b-a, border must be odd to obtain a valid alternating circle.
 
  """

  size = len(circle)

  #beginning of the initialization
  MCPS = list()
  for i in range(size):
    MCPS.append([])
    for j in range(size):
      MCPS[i].append(-np.inf)

  for i in range(size-1):
    MCPS[i][i+1] = 0  

  #end of the initialization

  for border in range(3, size, 2):
    #for every pair (a,b) with b-a = border
    for a in range(size-border):
      b = a + border
      #for every pair i<j in between a and b such that i-a, j-i and b-j are all odd
      for i in range(a+1,b,2):
        for j in range(i+1, b, 2):
          #identify a 2-break acting on these vertices
          tb = twobreakoncircle((a,i,j,b),circle)
          #update MCPS
          new_cost = MCPS[a][i] + MCPS[i][j] + MCPS[j][b] + parsimoniouscostofatb(tb)   
          if new_cost > MCPS[a][b]:
            MCPS[a][b] = new_cost 

  return MCPS[0][size-1] 


def parsimoniouscostofatb(tb):
  return -1
  

def twobreakoncircle(four,c):
  """
  Given four indexes a<i<j<b with i-a j-i and b-j being odd
  output a 2-break acting on the vertices of c indexed with a,i,j,b.
  
  If a is odd, then (c[a],c[i]) and (c[i],c[j]) are black edges,
  otherwise its (c[a],c[b]) and (c[i],c[j]) that are balck.

  This is because (c[0],c[1]) is a black edge in a circle.
  """

  (a,i,j,b) = four
  if a%2 == 0:
    return twobreaks.canonicaltb([[c[a],c[i]],[c[j],c[b]],[c[a],c[b]],[c[i],c[j]]])
  else:
    return twobreaks.canonicaltb([[c[a],c[b]],[c[i],c[j]],[c[a],c[i]],[c[b],c[j]]])
