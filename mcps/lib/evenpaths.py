#!/usr/bin/env python3
# Pijus Simonaitis                                               Winter 2018
#
"""
Library for bipartite graphs.


"""

import sys
import networkx as nx
import itertools
from collections import defaultdict
from . import breakpointgraph, mcps


def costsofevenpaths(AApathTOBBpathTOpairofcircles, g1, g2):
  """
  Computes mcps costs for all the circles obtained from the even length paths.  

  """ 

  AApathTOBBpathTOcost = defaultdict(dict)
  AApathTOBBpathTOcost['empty_a']['empty_b'] = 0

  for A in AApathTOBBpathTOpairofcircles:
    for B in AApathTOBBpathTOpairofcircles[A]:
      if A == 'empty_a' or B == 'empty_b':
        [nxc] = AApathTOBBpathTOpairofcircles[A][B]
        c = breakpointgraph.nxcircleTOcircle(nxc)    
        #cost = costfunc.parsimonious(c)
        AApathTOBBpathTOcost[A][B] = mcps.parsimoniousMCPS(c)
      else:
        #print("Lengths of AA and BB paths for which cost is being computed:",len(A.nodes()),len(B.nodes()))
        [nxc1,nxc2] = AApathTOBBpathTOpairofcircles[A][B]
        c1 = breakpointgraph.nxcircleTOcircle(nxc1)
        c2 = breakpointgraph.nxcircleTOcircle(nxc2)
        #cost1 = costfunc.parsimonious(c1)
        circleclost1 = mcps.parsimoniousMCPS(c1)
        #cost2 = costfunc.parsimonious(c2)
        circleclost2 = mcps.parsimoniousMCPS(c2)  
        AApathTOBBpathTOcost[A][B] = min(circleclost1, circleclost2)

  return AApathTOBBpathTOcost


def completebipartiteweightedgraph(AApathTOBBpathTOcost):
  """
  Constructs a bipartite weighted graph.
  Its vertices are AApaths and BBpaths. 
  There are enough 'epmty' paths (both AA and BB) added,
  that every AA and BB path could be matched with an 'empty' path.  

  """

  verticesA, verticesB = set(), set()
  vertexTOpath = defaultdict(dict)
  for A in AApathTOBBpathTOcost:
    if A != 'empty_a':
      verticesA.add(A)
      vertexTOpath[A] = A 
  
  for B in AApathTOBBpathTOcost['empty_a']:
    if B != 'empty_b':
      verticesB.add(B)
      vertexTOpath[B] = B

  lenA, lenB = len(verticesA), len(verticesB)

  for i in range(lenA):
    verticesB.add('empty_b_{}'.format(i))
    vertexTOpath['empty_b_{}'.format(i)] = 'empty_b'
  for i in range(lenB):
    verticesA.add('empty_a_{}'.format(i)) 
    vertexTOpath['empty_a_{}'.format(i)] = 'empty_a'
      
  Bipartite = nx.Graph()
  Bipartite.add_nodes_from(verticesA, bipartite=0)
  Bipartite.add_nodes_from(verticesB, bipartite=1)
  for (i,j) in itertools.product(verticesA,verticesB):
    Bipartite.add_edge(i,j, weight = AApathTOBBpathTOcost[vertexTOpath[i]][vertexTOpath[j]]) 
  return Bipartite, vertexTOpath

def maximumweightmatching(Bipartite):
  """
  Computes a maximum weight matching

  """
  return nx.algorithms.matching.max_weight_matching(Bipartite, maxcardinality=True)

def costofmatching(matching, AApathTOBBpathTOcost, vertexTOpath):
  """
  Computes the cost of a matching by
  adding the costs of the edges.

  """
  cost = 0
  for (i,j) in matching:
    if vertexTOpath[i] in AApathTOBBpathTOcost: 
      cost += AApathTOBBpathTOcost[vertexTOpath[i]][vertexTOpath[j]]
    elif vertexTOpath[j] in AApathTOBBpathTOcost:
      cost += AApathTOBBpathTOcost[vertexTOpath[j]][vertexTOpath[i]]
    else: 
      sys.exit('messed up aa and bb paths in a matching')
  return cost

def mcpsofevenpaths(AApathTOBBpathTOpairofcircles, g1, g2):
  print("Computing costs for the circles obtained from even paths")
  AApathTOBBpathTOcost = costsofevenpaths(AApathTOBBpathTOpairofcircles,g1, g2)
  print("Constructing bipartite graph") 
  B, vertexTOpath = completebipartiteweightedgraph(AApathTOBBpathTOcost)
  print("Looking for a maximum bipartite matching")
  matching = maximumweightmatching(B)
  return costofmatching(matching, AApathTOBBpathTOcost, vertexTOpath)
