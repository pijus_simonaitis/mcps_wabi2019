#!/usr/bin/env python3
# Pijus Simonaitis                                               Winter 2018
#
"""
Library containing functions for 2-breaks.
2-break is of a form [[a,b],[c,d],[a,b],[b,d]] or similar.

@note: For now I suppose that all the vertices in a 2-break are different.

"""

import itertools

#        _________________________
#_______/        Functions        \_____________________________________________

"""
def istb(tb):
  
  #Checks if tb is a 2-break

  

  if len(tb) != 4:
    return False

  for adjacency in tb:
    if len(adjacency) != 2:
      return False
  
 
  left = tb[0]+tb[1] 
  right = tb[2] + tb[3]
  left.sort()
  right.sort()
  if left != right:
    return False

  tb[0].sort()
  tb[2].sort()
  tb[3].sort()

  if tb[0] == tb[2] or tb[0] == tb[3]:
    return False
  return True

"""

def compareextremitities(a,b):
  telomers, nontelomers = [], []
  for i in (a,b):
    if i == 't1' or i == 't2':
      telomers.append(i)
    else:
      nontelomers.append(i)
  telomers.sort()
  nontelomers.sort()
  return telomers+nontelomers

def compareadjacencies(a,b,c,d):
  if a == 't1' or (a == 't2' and c != 't1'):
    return [(a,b),(c,d)]
  elif c == 't1' or c == 't2':
    return [(c,d),(a,b)]
  else:
    pair = [(a,b),(c,d)]
    pair.sort()
    return pair 

def canonicaltb(tb):
  """
  sorts [[a,b],[c,d],[e,f],[g,h]] and transforms it into a tuple tbc satisfying

  tbc[0] < tbc[1]
  tbc[2] < tbc[3]
  tbc[i][0] < tbc[i][i] 

  """

  for i in range(4):
    [tb[i][0], tb[i][1]] = compareextremitities(tb[i][0], tb[i][1])
  [tb[0], tb[1]] = compareadjacencies(tb[0][0], tb[0][1], tb[1][0], tb[1][1])
  [tb[2], tb[3]] = compareadjacencies(tb[2][0], tb[2][1], tb[3][0], tb[3][1]) 

  return tuple(tb)

def alltbs(nodes):
  """
  Generates all the 2-breaks (in their canonical form) operating on the given nodes

  """

  for (i,j,k,l) in itertools.combinations(nodes, 4):
    telomers, nontelomers = list(), list()
    for u in (i,j,k,l):
      if u != 't1' and u != 't2':
        nontelomers.append(u)  
      else:
        telomers.append(u) 
    nontelomers.sort()
    telomers.sort()
    [i,j,k,l] = telomers + nontelomers           
   

    yield ((i,j),(k,l),(i,k),(j,l))
    yield ((i,j),(k,l),(i,l),(j,k))
    yield ((i,k),(j,l),(i,j),(k,l))
    yield ((i,k),(j,l),(i,l),(j,k))
    yield ((i,l),(j,k),(i,j),(k,l))
    yield ((i,l),(j,k),(i,k),(j,l))


def tbtocircle(tb):
  ((i,j),(k,l),(i,k),(j,l)) = tb
  return [i,j,l,k]
